package Model;



public class Bibliotecario extends Usuario{
	
  private Usuario usuario;
  private String nome;
  private String telefone;
  private String email;
  private String endereco;

    public Bibliotecario(String umNomeBibliotecario, String umTelefoneBibliotecario, String umEmailBibliotecario, String umEnderecoBibliotecario) {
        
    }
  
    public Usuario getUsuario() {
	return usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        
	this.usuario = usuario;
    }
    public String getNome() {
        
	return nome;
    }
    public void setNome(String nome) {
        
	this.nome = nome;
    }
    public String getTelefone() {
        
	return telefone;
    }
    public void setTelefone(String telefone) {
        
	this.telefone = telefone;
    }
    public String getEmail() {
        
	return email;
    }
    public void setEmail(String email) {
        
	this.email = email;
    }
    public String getEndereco() {
        
	return endereco;
    }
    public void setEndereco(String endereco) {
        
	this.endereco = endereco;
    }
  
}
