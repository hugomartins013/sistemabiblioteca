package Model;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Autor{
    
    public static final int IDAT = 0;
    public static final int NAMEAT = 1;
    private String idAutor;
    private String nomeAutor;
    private static final ArrayList<Autor> listaAutores = new ArrayList<Autor>();

    public Autor(String umIdAutor, String umNomeAutor){
        
    }

    public String getIdAutor(){
        
        return idAutor;
    }

    public void setIdAutor(String idAutor){
        
        this.idAutor = idAutor;
    }

    public String getNomeAutor(){
        
        return nomeAutor;
    }

    public void setNomeAutor(String nomeAutor){
        
        this.nomeAutor = nomeAutor;
    }

    private boolean alterarCampoAutor(String nomeAutor, int qualCampo, String valor){
                    
        int i = 0;
        
        if((i = getIndexAutor(nomeAutor)) >= 0){
            
            Autor a = listaAutores.get(i);
            
            switch(qualCampo){
                
                case IDAT:{
                    
                    a.setIdAutor(valor);
                    return((listaAutores.set(i, a) != null));
                }
                case NAMEAT:{
                    
                   a.setNomeAutor(valor);
                    return((listaAutores.set(i, a) != null));
                }

                default:{
                    
                   return(Boolean.FALSE);
               }
            }
        }
        
        return(Boolean.FALSE);
        
    }

    public String adicionar(Autor umAutor){
        
        umAutor.setNomeAutor(umAutor.getNomeAutor().trim().toLowerCase());

        if (containsAutor(umAutor.getNomeAutor())){
            
            return ("Esse autor já está cadastrado");

        }

        if (listaAutores.add(umAutor)){
            
            return "Autor [" + umAutor.getNomeAutor() + "] adicionado com Sucesso!";
        }
        
        return "O autor não pode ser adicioado";

    }

    public String excluir(String umAutor){
        
        String mensagem = "Autor excluído com Sucesso!";
        int i = getIndexAutor(umAutor);
        
        if (i >= 0){
            
             if (listaAutores.remove(listaAutores.get(i))){
                 
                return mensagem;
             } 
             else{
                 
                return ("Autor sem index");
            }

        }
        else{
            
            System.out.println("Index retorado" + i);
        }

        return ("Aluno não encontrado");

    }

    public Autor pesquisarPorNome(String umNomeAutor){
        
        if (listaAutores == null || listaAutores.isEmpty()){
            
            System.out.println("Lista nula ou vazia");
            return (null);
        }

        for (Autor a : listaAutores){
            
            System.out.println("Procurando autor : " + umNomeAutor);
            System.out.println("Autor Atual= " + a.getNomeAutor());

            if (a.getNomeAutor().trim().equalsIgnoreCase(umNomeAutor.trim())){
                
                System.out.println("Autor Encontrado " + a.getNomeAutor());
                return (a);
            }


        }

        return null;
    }

    public String alterarNome(String nomePesquisa, String umNome){

        if (alterarCampoAutor(nomePesquisa, NAMEAT, umNome)){
            
            return ("Autor alterado com sucesso!");
        } 
        else{
            
            return ("Autor não pode ser alterado com sucesso!");
        }

    }

    private int getIndexAutor(String nomeAutor){
        
        for (int i = 0; i < listaAutores.size(); i++){
            
            if (listaAutores.get(i).getNomeAutor().trim().equalsIgnoreCase(nomeAutor.trim())){
                
                return (i);
            }
        }

        return (-1);
    }

    private boolean containsAutor(String nomeAutor){
        
        for (Autor a : listaAutores){
            
            if (a.getNomeAutor().equalsIgnoreCase(nomeAutor)){
                
                return (Boolean.TRUE);
            }
        }

        return (Boolean.FALSE);
    }

    public Autor(){
        
    }
}
