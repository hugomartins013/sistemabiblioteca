package Model;
import java.util.ArrayList;

public class Editora{
    
    public static final int IDED = 1;
    public static final int NAMEED = 0;
    private String idEditora;
    private String nomeEditora;
    private static final ArrayList<Editora> listaEditoras = new ArrayList<Editora>();

    public Editora() {
        
    }

    public Editora(String umIdEditora, String umNomeEditora) {
        
    }

	public String getIdEditora() {
            
		return idEditora;
	}
	public void setIdEditora(String idEditora) {
            
		this.idEditora = idEditora;
	}
	public String getNomeEditora() {
            
		return nomeEditora;
	}
	public void setNomeEditora(String nomeEditora) {
            
		this.nomeEditora = nomeEditora;
	}
	
        
    private boolean alterarCampoEditora(String nomeEditora, int qualCampo, String valor){
        
        int i = 0;
        
        if((i = getIndexEditora(nomeEditora)) >= 0){
            
            Editora e = listaEditoras.get(i);
            
            switch(qualCampo){
                
                case NAMEED:{
                    
                    e.setNomeEditora(valor);
                    return((listaEditoras.set(i, e) != null));
                }
                case IDED:{
                                     
                }

                default:{
                    
                   return(Boolean.FALSE);
               }
            }
        }
        
        return(Boolean.FALSE);
    }

    public String adicionar(Editora umaEditora) {
        
            umaEditora.setNomeEditora(umaEditora.getNomeEditora().trim().toLowerCase());
            
            if(containsEditora(umaEditora.getNomeEditora())){
                
                return("Essa editora já está cadastrada");
            }
            
            if(listaEditoras.add(umaEditora)){
                
                return "Editora["+ umaEditora.getNomeEditora()+"] adicionada com Sucesso!";
            }
            return "A editora não pode ser adicioada";
			
    }

    public String excluir(String umaEditora) {
        
	String mensagem = "Editora excluída com Sucesso!";
	int i = getIndexEditora(umaEditora);
        
        if(i >= 0){

            if(listaEditoras.remove(listaEditoras.get(i))){
                
                 return mensagem; 
            }
            else{
                
                 return("Editora sem index");
            }
		
        }
        else{
            
             System.out.println("Index retorado"+i);
        }
                
        return("Editora não encontrada");
			
    }

    public Editora pesquisarPorNome (String umNome) {
        
        if(listaEditoras == null || listaEditoras.isEmpty()){
            
                System.out.println("Lista nula ou vazia");
                return(null);
        }
            
        for(Editora a : listaEditoras){
            
                System.out.println("Procurando editora : "+umNome);
                System.out.println("Editora Atual= "+a.getNomeEditora());
                
            if(a.getNomeEditora().trim().equalsIgnoreCase(umNome.trim())){
                
                    System.out.println("Editora Encontrada "+a.getNomeEditora());
                    return(a);
            }
               
        }
            
            return null;
		
    }
		
    public String alterarNome (String nomePesquisa, String umNome){
			
        if(alterarCampoEditora(nomePesquisa, NAMEED, umNome)){
            
            return("Editora alterada com sucesso!");
        }
        else {
            
            return("Editora não pode ser alterada com sucesso!");
        }
			
			
    }
                
    private int getIndexEditora(String nomeEditora){
        
        for(int i = 0; i < listaEditoras.size(); i++){
            
            if(listaEditoras.get(i).getNomeEditora().trim().equalsIgnoreCase(nomeEditora.trim())){
                
                    return(i);
            }
        }
            
            return(-1);
    }
    private boolean containsEditora(String nomeEditora){
        
            for(Editora a : listaEditoras){
                
                if(a.getNomeEditora().equalsIgnoreCase(nomeEditora)){
                    return(Boolean.TRUE);
                }
            }
            
            return(Boolean.FALSE);
    }

   
}

