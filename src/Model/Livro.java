package Model;
import java.util.ArrayList;

public class Livro {
    public static final int ANOL = 2;
    public static final int IDIOMAL = 3;
    public static final int IDL = 0;
    public static final int TITULO = 0;
    public static final int TITULOL = 1;
    private String idLivro;
    private String titulo;
    private String ano;
    private String idioma;
    private  static final ArrayList<Livro> listaLivros = new ArrayList<Livro>();
    
    public Livro() {
    
    }

    public Livro(String umIdLivro, String umTituloLivro, String umAnoLivro, String umIdiomaLivro) {
        
    }

	public String getIdLivro() {
            
		return idLivro;
	}
	public void setIdLivro(String idLivro) {
            
		this.idLivro = idLivro;
	}
	public String getTitulo() {
            
		return titulo;
	}
	public void setTitulo(String titulo) {
            
		this.titulo = titulo;
	}
	public String getAno() {
            
		return ano;
	}
	public void setAno(String ano) {
            
		this.ano = ano;
	}
	public String getIdioma() {
            
		return idioma;
	}
	public void setIdioma(String idioma) {
            
		this.idioma = idioma;
	}
	
    private boolean alterarCampoLivro(String titulo, int qualCampo, String valor){
        
        int i = 0;
        
        if((i = getIndexLivro(titulo)) >= 0){
            
            Livro l = listaLivros.get(i);
            
            switch(qualCampo){
                
                case IDL:{
                    
                    l.setIdLivro(valor);
                    return((listaLivros.set(i, l) != null));
                }
                case TITULOL:{
                    
                   l.setTitulo(valor);
                    return((listaLivros.set(i, l) != null));
                }
                 case ANOL:{
                     
                   l.setAno(valor);
                    return((listaLivros.set(i, l) != null));
                }
                case IDIOMAL:{
                    
                   l.setIdioma(valor);
                    return((listaLivros.set(i, l) != null));
                }

                
               default:{
                   return(Boolean.FALSE);
               }
            }
        }
        
        return(Boolean.FALSE);
    }

    public String adicionar(Livro umLivro) {
        
            umLivro.setIdLivro(umLivro.getTitulo().trim().toLowerCase());
            
        if(containsLivro(umLivro.getTitulo())){
            
                return("Esse livro já está cadastrado");
  
        }
            
        if(listaLivros.add(umLivro)){
            
                return "Livro ["+ umLivro.getTitulo()+"] adicionado com Sucesso!";
            }
        
        return "O livro não pode ser adicioado";
			
    }

    public String excluir(String umLivro) {
        
        String mensagem = "Livro excluído com Sucesso!";
	int i = getIndexLivro(umLivro);
        
        if(i >= 0){

            if(listaLivros.remove(listaLivros.get(i))){
                
                       return mensagem; 
            }
            else{
                
                return("Livro sem index");
            }
		
        }
        else{
            
            System.out.println("Index retorado"+i);
        }
                
        return("Livro não encontrado");
			
        }

        public Livro pesquisarTitulo (String umTitulo) {
            
            if(listaLivros == null || listaLivros.isEmpty()){
                
                System.out.println("Lista nula ou vazia");
                return(null);
            }
            
            for(Livro l : listaLivros){
                
                System.out.println("Procurando livro : "+umTitulo);
                System.out.println("Livro Atual= "+l.getTitulo());
                
                if(l.getTitulo().trim().equalsIgnoreCase(umTitulo.trim())){
                
                    System.out.println("Livro Encontrado "+l.getTitulo());
                    return(l);
                }
               
            }
            
            return null;
        }

        public String alterarTitulo (String nomePesquisa, String umTitulo){
	
            if(alterarCampoLivro(nomePesquisa, TITULOL, umTitulo)){
                
            return("Livro alterado com sucesso!");
            }
            else {
                
            return("Livro não pode ser alterado com sucesso!");
            }
	
	
	}

        public String alterarAno (String nomePesquisa, String umAno){
	
            if(alterarCampoLivro(nomePesquisa, ANOL, umAno)){
                
            return("Livro alterado com sucesso!");
            }
            else {
                
            return("Livro não pode ser alterado com sucesso!");
            }
	
	}

        public String alterarIdioma (String nomePesquisa, String umIdioma){
	
            if(alterarCampoLivro(nomePesquisa, IDIOMAL, umIdioma)){
            return("Livro alterado com sucesso!");
            }
            else {
                
            return("Livro não pode ser alterado com sucesso!");
            }
	
	}

        private int getIndexLivro(String titulo){
            
            for(int i = 0; i < listaLivros.size(); i++){
                
                if(listaLivros.get(i).getTitulo().trim().equalsIgnoreCase(titulo.trim())){
                    
                    return(i);
                }
            }
            
            return(-1);
        }
        
        private boolean containsLivro(String titulo){
            
            for(Livro l : listaLivros){
                
                if(l.getTitulo().equalsIgnoreCase(titulo)){
                    
                    return(Boolean.TRUE);
                }
            }
            
            return(Boolean.FALSE);
        }


}
