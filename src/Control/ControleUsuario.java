package Control;
import java.util.ArrayList;
import Model.Aluno;
import Model.Bibliotecario;

public class ControleUsuario {
    public static final int EMAIL = 1;
    public static final int EMAILBL = 1;
    public static final int ENDERECO = 2;
    public static final int ENDERECOBL = 2;
    public static final int MATRICULA = 4;
    public static final int NAME = 0;
    public static final int NAMEBL = NAME;
    public static final int NOME = 0;
    public static final int TELEFONE = 3;
    public static final int TELEFONEBL = 3;
    private static final ArrayList<Aluno> listaAlunos = new ArrayList<Aluno>();
    private static final ArrayList<Bibliotecario> listaBibliotecarios = new ArrayList<Bibliotecario>();


    public ArrayList<Aluno> getListaAlunos(){
        
        return listaAlunos;
    }

    public ArrayList<Bibliotecario> getListaBibliotecarios(){
        
        return listaBibliotecarios;
    }

    public String adicionar(Aluno umAluno){

        umAluno.setNome(umAluno.getNome().trim().toLowerCase());

        if (containsAluno(umAluno.getNome())){
            
            return ("Esse aluno ja esta cadastrado");
        }

        if (listaAlunos.add(umAluno)){
            
            return "Aluno [" + umAluno.getNome() + "] adicionado com Sucesso!";
        }
        
        return "O Aluno não pode ser adicioado";
    }

    public String excluir(String umAluno){
        
        String mensagem = "Aluno excluído com Sucesso!";
        int i = getIndexAluno(umAluno);
        
        if (i >= 0){

            if (listaAlunos.remove(listaAlunos.get(i))){
                
                return mensagem;
            }
            
            else{
                
                return ("Aluno sem index");
            }

        } 
        
        else{
            
            System.out.println("Index retorado" + i);
        }

        return ("Aluno não encontrado");

    }

    private boolean containsAluno(String nome){
        
        for (Aluno a : listaAlunos){
            
            if (a.getNome().equalsIgnoreCase(nome)){
                
                return (Boolean.TRUE);
            }
        }

        return (Boolean.FALSE);
    }

    private Aluno getAluno(String nome){
        
        for (Aluno a : listaAlunos){
            
            if (a.getNome().trim().equalsIgnoreCase(nome.trim())){
                
                return (a);
            }
        }

        return (null);
    }

    private int getIndexAluno(String nome){
        
        for (int i = 0; i < listaAlunos.size(); i++){
            
            if (listaAlunos.get(i).getNome().trim().equalsIgnoreCase(nome.trim())){
                
                return (i);
            }
        }

        return (-1);
    }

    private boolean alterarCampo(String nomeAluno, int qualCampo, String valor){
        
        int i = 0;
        
        if((i = getIndexAluno(nomeAluno)) >= 0){
            
            Aluno al = listaAlunos.get(i);
            
            switch(qualCampo){
                
                case NOME:{
                    
                    al.setNome(valor);
                    return((listaAlunos.set(i, al) != null));
                }
                case EMAIL:{
                    
                    al.setEmail(valor);
                    return((listaAlunos.set(i, al) != null));
                }
                case ENDERECO:{
                    
                    al.setEndereco(valor);
                    return((listaAlunos.set(i, al) != null));
                }
                case TELEFONE:{
                    al.setTelefone(valor);
                    return((listaAlunos.set(i, al) != null));
                }
                case MATRICULA:{
                    
                    al.setMatricula(valor);
                    return((listaAlunos.set(i, al) != null));
                }
                
                default:{
                   
                    return(Boolean.FALSE);
               }
            }
        }
        
        return(Boolean.FALSE);
    }
    
    private boolean alterarCampoBibl(String nomeBibl, int qualCampo, String valor){
        
        int i = 0;
        
        if((i = getIndexBibliotecario(nomeBibl)) >= 0){
            
            Bibliotecario bl = listaBibliotecarios.get(i);
            
            switch(qualCampo){
                
                case NAMEBL:{
                    
                    bl.setNome(valor);
                    return((listaBibliotecarios.set(i, bl) != null));
                }
                case EMAILBL:{
                    
                    bl.setEmail(valor);
                    return((listaBibliotecarios.set(i, bl) != null));
                }
                case ENDERECOBL:{
                    
                    bl.setEndereco(valor);
                    return((listaBibliotecarios.set(i, bl) != null));
                }
                case TELEFONEBL:{
                    
                    bl.setTelefone(valor);
                    return((listaBibliotecarios.set(i, bl) != null));
                }

                default:{
                    
                    return(Boolean.FALSE);
               }
            }
        }
        
        return(Boolean.FALSE);
    }
    
    public Aluno pesquisarPorNomeAluno(String umNome) {

        if (listaAlunos == null || listaAlunos.isEmpty()){
            
            System.out.println("Lista nula ou vazia");
            return (null);
        }

        for (Aluno a : listaAlunos){
            
            System.out.println("Procurando aluno : " + umNome);
            System.out.println("Aluno Atual= " + a.getNome());

            if(a.getNome().trim().equalsIgnoreCase(umNome.trim())){
                
                System.out.println("Aluno Encontrado " + a.getNome());
                return (a);
            }

        }

        return null;
    }

    public String alterarNome(String nomePesquisa, String umNome){

        if(alterarCampo(nomePesquisa, NOME, umNome)){
            
            return("Aluno alterado com sucesso!");
        }
        
        else{
            
            return("Aluno não pode ser alterado com sucesso!");
        }
        
    }

    public String alterarMatricula(String nomePesquisa, String umaMatricula){

        if(alterarCampo(nomePesquisa, MATRICULA, umaMatricula)){
            
            return("Aluno alterado com sucesso!");
        }
        
        else{
            
            return("Aluno não pode ser alterado com sucesso!");
        }

    }

    public String alterarTelefone(String nomePesquisa, String umTelefone){

        if(alterarCampo(nomePesquisa, TELEFONE, umTelefone)){
            
            return("Aluno alterado com sucesso!");
        }
        
        else{
            
            return("Aluno não pode ser alterado com sucesso!");
        }

    }

    public String alterarEmail(String nomePesquisa, String umEmail){

        if(alterarCampo(nomePesquisa, EMAIL, umEmail)){
            
            return("Aluno alterado com sucesso!");
        }
        else{
            
            return("Aluno não pode ser alterado com sucesso!");
        }

    }

    public String alterarEndereco(String nomePesquisa, String umEndereco){

        if(alterarCampo(nomePesquisa, ENDERECO, umEndereco)){
            
            return("Aluno alterado com sucesso!");
        }
        else{
            
            return("Aluno não pode ser alterado com sucesso!");
        }


    }
	
    public String adicionar(Bibliotecario umBibliotecario){

        umBibliotecario.setNome(umBibliotecario.getNome().trim().toLowerCase());

        if (containsBibliotecario(umBibliotecario.getNome())){
            
            return ("Esse bibliotecário já está cadastrado");
        }

        if (listaBibliotecarios.add(umBibliotecario)){
            
            return "Bibliotecário [" + umBibliotecario.getNome() + "] adicionado com Sucesso!";
        }
        
        return "O bibliotecario não pode ser adicioado";
    }

    public String excluirBibliotecario(String umBibliotecario){
        
        String mensagem = "Bibliotecário excluído com Sucesso!";
        int i = getIndexBibliotecario(umBibliotecario);
        
        if (i >= 0){

            if (listaBibliotecarios.remove(listaBibliotecarios.get(i))){
                
                return mensagem;
            } 
            
            else{
                
                return ("Bibliotecário sem index");
            }

        } 
        
        else{
            
            System.out.println("Index retorado" + i);
        }

        return("Bibliotecário não encontrado");

    }

    public Bibliotecario pesquisarPorNomeBibliotecario(String umNome){
        
        if (listaBibliotecarios == null || listaBibliotecarios.isEmpty()){
            
            System.out.println("Lista nula ou vazia");
            return (null);
        }

        for(Bibliotecario a : listaBibliotecarios){
            
            System.out.println("Procurando bibliotecario : " + umNome);
            System.out.println("Bibliotecario Atual= " + a.getNome());

            if(a.getNome().trim().equalsIgnoreCase(umNome.trim())){
                
                System.out.println("Bibliotecario Encontrado " + a.getNome());
                return (a);
            }

        }

        return null;
    }

    public String alterarNomeBibl(String nomePesquisa, String umNome){

        if(alterarCampoBibl(nomePesquisa, NAMEBL, umNome)){
            
            return("Bibliotecário alterado com sucesso!");
        }
        
        else{
            
            return("Bibliotecário não pode ser alterado com sucesso!");
        }


    }

    public String alterarTelefoneBibl(String nomePesquisa, String umTelefone){

        if(alterarCampoBibl(nomePesquisa, TELEFONEBL, umTelefone)){
            
            return("Bibliotecário alterado com sucesso!");
        }
        
        else{
            
            return("Bibliotecário não pode ser alterado com sucesso!");
        }

    }

    public String alterarEmailBibl(String nomePesquisa, String umEmail){

        if(alterarCampoBibl(nomePesquisa, EMAILBL, umEmail)){
            
            return("Bibliotecário alterado com sucesso!");
        }
        
        else{
            
            return("Bibliotecário não pode ser alterado com sucesso!");
        }

    }

    public String alterarEnderecoBibl(String nomePesquisa, String umEndereco){

        if(alterarCampoBibl(nomePesquisa, ENDERECOBL, umEndereco)){
            
            return("Bibliotecário alterado com sucesso!");
        }
        
        else{
            
            return("Bibliotecário não pode ser alterado com sucesso!");
        }

    }

    private int getIndexBibliotecario(String nome){
        
        for (int i = 0; i < listaBibliotecarios.size(); i++){
            
            if (listaBibliotecarios.get(i).getNome().trim().equalsIgnoreCase(nome.trim())){
                
                return (i);
            }
        }

        return (-1);
    }

    private boolean containsBibliotecario(String nome){
        
        for (Bibliotecario a : listaBibliotecarios){
           
            if (a.getNome().equalsIgnoreCase(nome)){
                
                return (Boolean.TRUE);
            }
        }

        return (Boolean.FALSE);
    }

    public ControleUsuario(){
        
    }
}
