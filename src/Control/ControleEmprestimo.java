package Control;
import java.util.ArrayList;
import Model.Emprestimo;

public class ControleEmprestimo {
    public static final int DATADEMP = 2;
    public static final int DATAEMP = 1;
    public static final int IDEMP = 0;
		

private static final ArrayList<Emprestimo> listaEmprestimos = new ArrayList<Emprestimo>();

    public ArrayList<Emprestimo> getListaEmprestimos() {
	return listaEmprestimos;
    }

    public String realizar(Emprestimo umEmprestimo) {
        
	umEmprestimo.setIdEmprestimo(umEmprestimo.getIdEmprestimo().trim().toLowerCase());
            
        if(containsEmprestimo(umEmprestimo.getIdEmprestimo())){
            
                return("Esse esmpréstimo já foi realizado.");
        }
            
        if(listaEmprestimos.add(umEmprestimo)){
            
                return "Emprestimo ["+ umEmprestimo.getIdEmprestimo()+"] realizado com Sucesso!";
        }
        
                return "O empréstimo não pode ser realizado.";
				
    }

    public String finalizar(String umEmprestimo) {
        
	String mensagem = "Empréstimo finalizado com Sucesso!";
	int i = getIndexEmprestimo(umEmprestimo);
        
        if(i >= 0){

            if(listaEmprestimos.remove(listaEmprestimos.get(i))){
                
                       return mensagem; 
            }
            else{
                
                       return("Emprestimo sem index");
            }
		
        }
        else{
            
            System.out.println("Index retorado"+i);
        }
                
                return("Emprestimo não encontrado");
				
    }
			
    public Emprestimo pesquisarPorId (String umId){
        
        if(listaEmprestimos == null || listaEmprestimos.isEmpty()){
            
                System.out.println("Lista nula ou vazia");
                return(null);
        }
            
        for(Emprestimo em : listaEmprestimos){
            
                System.out.println("Procurando emprestimo : "+umId);
                System.out.println("Emprestimo Atual= "+em.getIdEmprestimo());
                
            if(em.getIdEmprestimo().trim().equalsIgnoreCase(umId.trim())){
                
                    System.out.println("Emprestimo Encontrado "+em.getIdEmprestimo());
                    return(em);
            }
               
         }
            
            return null;
    }
    
    private boolean alterarCampoEmprestimo(String data, int qualCampo, String valor){
        
        int i = 0;
        
        if((i = getIndexEmprestimo(data)) >= 0){
            
            Emprestimo em = listaEmprestimos.get(i);
            
            switch(qualCampo){
                
                case IDEMP:{
                    em.setIdEmprestimo(valor);
                    return((listaEmprestimos.set(i, em) != null));
                }
                    
                case DATAEMP:{
                    em.setDataEmprestimo(valor);
                    return((listaEmprestimos.set(i, em) != null));
                }
                    
                case DATADEMP:{
                   em.setDataDevolucao(valor);
                    return((listaEmprestimos.set(i, em) != null));
                }

                
               default:{
                   return(Boolean.FALSE);
               }
            }
        }
        
        return(Boolean.FALSE);
    }
			
    public String renovarEmprestimo (String idPesquisa, String umaData){
				
	if(alterarCampoEmprestimo(idPesquisa, DATADEMP, umaData)){
            
            return("Empréstimo renovado com sucesso!");
        }
        else{
            
            return("Empréstimo não pode ser renovado com sucesso!");
        }
				
				
    }
                        
    private int getIndexEmprestimo(String idEmprestimo){
        
        for(int i = 0; i < listaEmprestimos.size(); i++){
            
            if(listaEmprestimos.get(i).getIdEmprestimo().trim().equalsIgnoreCase(idEmprestimo.trim())){
                
                    return(i);
            }
        }
            
        return(-1);
    }
     private boolean containsEmprestimo(String id){
         
            for(Emprestimo em : listaEmprestimos){
                
                if(em.getIdEmprestimo().equalsIgnoreCase(id)){
                    
                    return(Boolean.TRUE);
                }
            }
            
            return(Boolean.FALSE);
    }     
                        
    public ControleEmprestimo() {

    }
    
    public ControleEmprestimo(String umIdEmprestimo, String umaDataEmprestimo, String umaDataDevolucaoEmprestimo) {
        
    }
			
}
